import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeftMenuComponent } from './left-menu/left-menu.component';
import { TopMenuComponent } from './top-menu/top-menu.component';
import {MatDividerModule, MatListModule, MatToolbarModule} from '@angular/material';
import {SharedModule} from '../shared/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuardService} from './auth-guard/auth-guard.service';

@NgModule({
  declarations: [TopMenuComponent, LeftMenuComponent ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatListModule,
    MatDividerModule,
    SharedModule,
    RouterModule.forChild([])
  ],
  exports: [TopMenuComponent, LeftMenuComponent ],
  providers: [AuthGuardService]
})
export class CoreModule { }
