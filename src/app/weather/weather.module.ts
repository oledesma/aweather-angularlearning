import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {WeatherCardComponent} from './weather-card/weather-card.component';
import {WheaterForecastComponent} from './wheater-forecast/wheater-forecast.component';
import {WheaterSearchComponent} from './wheater-search/wheater-search.component';
import {WeatherService} from './services/weather.service';
import {WeatherRoutingModule} from './weather-routing.module';

@NgModule({
  declarations: [WeatherCardComponent, WheaterForecastComponent, WheaterSearchComponent],
  imports: [
    CommonModule,
    SharedModule,
    WeatherRoutingModule
  ],
  exports: [WeatherCardComponent, WheaterSearchComponent],
  providers: [WeatherService]
})
export class WeatherModule { }
