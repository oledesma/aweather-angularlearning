import {Component, Input, OnInit} from '@angular/core';
import {Forecast} from '../forecast.model';

@Component({
  selector: 'app-wheater-forecast',
  templateUrl: './wheater-forecast.component.html',
  styleUrls: ['./wheater-forecast.component.css']
})
export class WheaterForecastComponent implements OnInit {

  @Input() forecast: Array<Forecast>;
  constructor() { }

  ngOnInit() {
  }

}
