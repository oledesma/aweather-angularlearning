import {Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-wheater-search',
  templateUrl: './wheater-search.component.html',
  styleUrls: ['./wheater-search.component.css']
})
export class WheaterSearchComponent implements OnInit {

  @Output() searchEvent: EventEmitter<string> = new EventEmitter<string>();

  loading: boolean;
  cityName: string;

  constructor() { }

  ngOnInit() {
    this.loading = false;
    this.cityName = '';
  }

  public searchCity() {
    this.loading = !this.loading;
    this.searchEvent.emit(this.cityName);
  }
}
