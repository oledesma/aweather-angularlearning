import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-wheater-icon',
  templateUrl: './wheater-icon.component.html',
  styleUrls: ['./wheater-icon.component.css']
})
export class WheaterIconComponent implements OnInit {

  @Input() wheaterInfo: string;
  constructor() { }

  ngOnInit() {
  }

}
