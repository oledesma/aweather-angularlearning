import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialFormsModule} from './material-forms/material-forms.module';
import { WheaterIconComponent } from './wheater-icon/wheater-icon.component';
import { DropdownDirective } from './dropdown.directive';
import { DropdowntoggleDirective } from './dropdowntoggle.directive';
import { CelsiusPipe } from './pipes/celsius.pipe';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [WheaterIconComponent, DropdownDirective, DropdowntoggleDirective, CelsiusPipe],
  imports: [
    CommonModule,
    MaterialFormsModule
  ],
  exports: [MaterialFormsModule, WheaterIconComponent, DropdownDirective, DropdowntoggleDirective, CelsiusPipe, HttpClientModule]
})
export class SharedModule { }
